-- suppression des tables
DROP TABLE IF EXISTS etudiants;

-- création des tables
CREATE TABLE etudiants (
  id SMALLSERIAL,
  nom VARCHAR(20) NOT NULL,
  prenom VARCHAR(20),
  login VARCHAR(8),
  PRIMARY KEY(id)
);

-- insertion des valeurs
INSERT INTO etudiants (nom, prenom) VALUES
('Tux', 'Camille'),
('Black', 'Francis');
