"""
    gestion_etudiants
    ~~~~~~~~~~~~~~~~~

    Le module implémente la gestion de la base PostgreSql.

    :copyright: (c) 2015 par moulinux.
    :license: GPLv3, voir la LICENCE pour plus d'informations.
    .. todo:: Implémenter le reste des méthodes CRUD.
"""
import psycopg2
import psycopg2.extras
import config


class GestionBd:
    """Mise en place et interfaçage d'une base de données PostgreSQL"""

    def __init__(self):
        """Établissement de la connexion - Création du curseur"""
        try:
            self.conn = \
                psycopg2.connect(dbname=config.DB_NAME,
                                 user=config.USER, password=config.PASSWD,
                                 host=config.HOST)
        except psycopg2.DatabaseError as err:
            print('La connexion avec la base de données a échoué :\n',
                  'Erreur détectée :{}'.format(err))
            self.echec = True
        else:
            # création d'un curseur de type dictionnaire :
            self.conn.autocommit = True
            self.cursor = \
                self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            self.echec = False

    def executer_req(self, req, params):
        """Exécution de la requête <req>, avec détection d'erreur éventuelle"""
        try:
            self.cursor.execute(req, params)
        except Exception as err:
            # afficher la requête et le message d'erreur système :
            print("Requête SQL incorrecte :\n{}\nErreur détectée :{}"
                  .format(req, err))
            return False
        else:
            return True

    def resultat_req(self):
        """
            renvoie le résultat de la requête précédente (une liste de tuples)
        """
        return self.cursor.fetchall()

    def valider(self):
        """transfert du curseur vers le disque"""
        if self.conn:
            self.conn.commit()

    def fermer(self):
        """fermeture du curseur"""
        if self.conn:
            self.cursor.close()
            self.conn.close()

    def conn_base(self):
        """
            indique si la connexion a réussie ou échouée.

            :return: True si la connexion a échouée, False sinon
            :rtype: booléen
        """
        return self.echec

    def obtenir_etudiants(self):
        """
            Retourne le jeu des enregistrements des étudiants.

            :return: jeu des enregistrements des étudiants
            :rtype: list
        """
        reqSql = """SELECT nom, prenom FROM etudiants
                    ORDER BY id"""
        if self.executer_req(reqSql, ()):
            return self.resultat_req()
        return None
