# Gestionnaire d'étudiants (démo)
L'application implémente une interface Web à une base de données PostgreSQL.

> Dans cette première itération, seul la liste des étudiants est traitée.
D'autres itérations permettront d'implémenter d'autres accès CRUD.

## Objectifs

Travailler autour des bases de la programmation web CGI,
l'accès aux bases de données en Python et la documentation technique

## La mission

Suite à des erreurs signalées par plusieurs agents stagiaires,
le responsable des formations, observe qu’actuellement la saisie
d'une nouvelle action de formation n’est pas toujours fiable,
il souhaite que l’application soit mise à jour pour qu’elle vérifie
la vraisemblance des informations saisies par les utilisateurs.

## Environnement technique

L'application reposant sur une base de données PostgreSql, il est bien
sûr nécessaire d'installer ce service. L'accès à la base se configure
dans le fichier `config.py`.

Sur une distribution GNU/Linux Debian, l'installation des paquets
suivants est nécessaire:

`apt -y install python3-psycopg2 python3-jinja2`

Afin de travailler sur le respect des normes et standards, propres à
Python (PEP 8) et la documentation technique :

`apt -y install python3-pycodestyle python3-sphinx python3-stemmer`

## Documentation technique

L'application dispose d'une documentation technique qui a été
générée avec Sphinx, elle est accessible [en ligne][doc]
(via les pages de framagit et l'intégration continue).

[doc]: https://s2sio.frama.io/web/cgi_etudiants
