#! /usr/bin/env python3
"""
    etudiants.app
    ~~~~~~~~~~~~~

    Module principal de l'application de gestion des étudiants.

    :copyright: (c) 2021 par moulinux.
    :license: GPLv3, voir la LICENCE pour plus d'informations.
    .. todo:: Implémenter les actions aux méthodes CRUD.
"""
import cgitb
import sys
import codecs           # configuration de l'encodage des caractères
import cgi              # Module d'interface avec le serveur Web
from jinja2 import Template
from pathlib import Path
from gestion_etudiants import *
import dict_fr as lang

cgitb.enable()
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

print("Content-Type: text/html; charset=utf-8\n")
bdd = GestionBd()

if bdd.conn_base():
    print(lang.dico['errConnBdd'])
else:
    chemin = Path('./templates/home.html')
    with chemin.open("r") as mon_fichier:
        template = Template(mon_fichier.read())

    print(template.render(titre=lang.dico['lstEtudiants'],
                          nom=lang.dico['lbNom'],
                          prenom=lang.dico['lbPrenom'],
                          les_etudiants=bdd.obtenir_etudiants()))

    bdd.valider()
    bdd.fermer()
