Documentation technique
=======================

.. automodule:: app
    :members:

.. automodule:: gestion_etudiants
.. autoclass:: GestionBd
    :members:
    :private-members:
    :special-members:
